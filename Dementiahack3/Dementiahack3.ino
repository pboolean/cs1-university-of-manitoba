/*

Run this sketch on two Zigduinos, open the serial monitor at 9600 baud, and type in stuff
Watch the Rx Zigduino output what you've input into the serial port of the Tx Zigduino

*/

#include <ZigduinoRadio.h>
#include <stdint.h>

#include <HTU21D.h>
#include <Wire.h>
#include "Adafruit_SI1145.h"

HTU21D humiditySensor;

int nodeid = 1;

int serverid = 19;

String inputString = "";         // a string to hold incoming data
boolean stringComplete = false;  // whether the string is complete

int values[4] = { 0, 0, 0, 0 };
int tmpvalues[4] = { 0, 0, 0, 0 };
String serialDataIn = "";
int counter = 0;
boolean closing = false;
char bytes[512];

const int numReadings = 30;

int readings[numReadings];      // the readings from the analog input
int readIndex = 0;              // the index of the current reading
int total = 0;                  // the running total
int average = 0;                // the average



#define BNO055_I2C_H_SLAVE_ADDR   0x29
#define BNO055_I2C_ADDR   0x28
#define BNO055_I2C_HID_SLAVE_ADDR 0x40

// BNO055 Registers
#define MAG_RADIUS_MSB 0x6A // R/W
#define MAG_RADIUS_LSB 0x69 // R/W
#define ACC_RADIUS_MSB 0x68 // R/W
#define ACC_RADIUS_LSB 0x67 // R/W

#define GYR_OFFSET_Z_MSB 0x66 // R/W
#define GYR_OFFSET_Z_LSB 0x65 // R/W
#define GYR_OFFSET_Y_MSB 0x64 // R/W
#define GYR_OFFSET_Y_LSB 0x63 // R/W 
#define GYR_OFFSET_X_MSB 0x62 // R/W
#define GYR_OFFSET_X_LSB 0x61 // R/W

#define MAG_OFFSET_Z_MSB 0x60 // R/W
#define MAG_OFFSET_Z_LSB 0x5F // R/W
#define MAG_OFFSET_Y_MSB 0x5E // R/W
#define MAG_OFFSET_Y_LSB 0x5D // R/W
#define MAG_OFFSET_X_MSB 0x5C // R/W
#define MAG_OFFSET_X_LSB 0x5B // R/W

#define ACC_OFFSET_Z_MSB 0x5A // R/W
#define ACC_OFFSET_Z_LSB 0x59 // R/W
#define ACC_OFFSET_Y_MSB 0x58 // R/W
#define ACC_OFFSET_Y_LSB 0x57 // R/W
#define ACC_OFFSET_X_MSB 0x56 // R/W
#define ACC_OFFSET_X_LSB 0x55 // R/W

// Registers 43-54 reserved for Soft Iron Calibration Matrix

#define AXIS_MAP_SIGN   0x42 // Bits0-2: R/W; Bits3-7: reserved 
#define AXIS_MAP_CONFIG 0x41 // Bits0-5: R/W; Bits6-7: reserved
#define TEMP_SOURCE     0x40 // Bits0-1: R/W; Bits2-7: reserved

#define SYS_TRIGGER 0x3F
#define PWR_MODE    0x3E
#define OPR_MODE    0x3D //default = 0x1C

// Register 3C Reserved

#define UNIT_SEL       0x3B //RO
#define SYS_ERR        0x3A //RO
#define SYS_STATUS     0x39 //RO 
#define SYS_CLK_STATUS 0x38 //RO
#define INT_STATUS     0x37	//RO
#define ST_RESULT      0x36 //RO
#define CALIB_STAT     0x35 //RO
#define TEMP           0x34 //RO

#define GRV_DATA_Z_MSB 0x33 //RO
#define GRV_DATA_Z_LSB 0x32 //RO
#define GRV_DATA_Y_MSB 0x31 //RO
#define GRV_DATA_Y_LSB 0x30 //RO
#define GRV_DATA_X_MSB 0x2F //RO
#define GRV_DATA_X_LSB 0x2E //RO

#define LIA_DATA_Z_MSB 0x2D //RO
#define LIA_DATA_Z_LSB 0x2C //RO
#define LIA_DATA_Y_MSB 0x2B //RO
#define LIA_DATA_Y_LSB 0x2A //RO
#define LIA_DATA_X_MSB 0x29 //RO
#define LIA_DATA_X_LSB 0x28 //RO

#define QUA_DATA_Z_MSB 0x27 //RO
#define QUA_DATA_Z_LSB 0x26 //RO
#define QUA_DATA_Y_MSB 0x25 //RO
#define QUA_DATA_Y_LSB 0x24 //RO
#define QUA_DATA_X_MSB 0x23 //RO
#define QUA_DATA_X_LSB 0x22 //RO
#define QUA_DATA_W_MSB 0x21 //RO
#define QUA_DATA_W_LSB 0x20 //RO

#define EUL_PITCH_MSB   0x1F //RO
#define EUL_PITCH_LSB   0x1E //RO
#define EUL_ROLL_MSB    0x1D //RO
#define EUL_ROLL_LSB    0x1C //RO
#define EUL_HEADING_MSB 0x1B //RO
#define EUL_HEADING_LSB 0x1A //RO

#define GYR_DATA_Z_MSB 0x19 //RO
#define GYR_DATA_Z_LSB 0x18 //RO
#define GYR_DATA_Y_MSB 0x17 //RO
#define GYR_DATA_Y_LSB 0x16 //RO
#define GYR_DATA_X_MSB 0x15 //RO
#define GYR_DATA_X_LSB 0x14 //RO

#define MAG_DATA_Z_MSB 0x13 //RO
#define MAG_DATA_Z_LSB 0x12 //RO
#define MAG_DATA_Y_MSB 0x11 //RO
#define MAG_DATA_Y_LSB 0x10 //RO
#define MAG_DATA_X_MSB 0x0F //RO
#define MAG_DATA_X_LSB 0x0E //RO

#define ACC_DATA_Z_MSB 0x0D //RO
#define ACC_DATA_Z_LSB 0x0C //RO
#define ACC_DATA_Y_MSB 0x0B //RO
#define ACC_DATA_Y_LSB 0x0A //RO
#define ACC_DATA_X_MSB 0x09 //RO
#define ACC_DATA_X_LSB 0x08 //RO

#define PAGE_ID       0x07 //RO
#define BL_REV_ID     0x06 //RO
#define SW_REV_ID_MSB 0x05 //RO
#define SW_REV_ID_LSB 0x04 //RO

#define GYR_ID  0x03 // RO; Default = 0x0F
#define MAG_ID  0x02 // RO; Default = 0x32
#define ACC_ID  0xFB // RO; Default = 0xFB
#define CHIP_ID 0x00 // RO; Default 0xA0

// PAGE 1 (All unreserved registers in Page 1 are READ ONLY)
// Registers 7F-60 reserved
// Registers 5F-50 are named UNIQUE_ID and are READ ONLY
// Registers 4F-20 reserved

#define GYR_AM_SET     0x1F // Default = 0x0A
#define GYR_AM_THRES   0x1E	// Default = 0x04
#define GYR_DUR_Z      0x1D	// Default = 0x19
#define GYR_HR_Z_SET   0x1C	// Default = 0x01
#define GYR_DUR_Y      0x1B	// Default = 0x19
#define GYR_HR_Y_SET   0x1A	// Default = 0x01
#define GYR_DUR_X      0x19	// Default = 0x19
#define GYR_HR_X_SET   0x18	// Default = 0x01
#define GYR_INT_SETING 0x17 // Default = 0x00

#define ACC_NM_SET       0x16 // Default = 0x0B
#define ACC_NM_THRE      0x15 // Default = 0x0A
#define ACC_HG_THRE      0x14 // Default = 0xC0
#define ACC_HG_DURATION  0x13 // Default = 0x0F
#define ACC_INT_SETTINGS 0x12 // Default = 0x03
#define ACC_AM_THRES     0x11 //Default = 0x14

#define INT_EN   0x10
#define INT_MASK 0x0F

// Register 0x0E reserved

#define GYR_SLEEP_CONFIG 0x0D 
#define ACC_SLEEP_CONFIG 0x0C
#define GYR_CONFIG_1     0x0B
#define GYR_CONFIG_0     0x0A
#define MAG_CONFIG       0x09
#define ACC_CONFIG       0x08

// PAGE_ID still mapped to 0x07
// Registers 6-0 reserved

#define PWR_MODE_SEL_BIT    0
#define PWR_MODE_SEL_LENGTH 2

#define PWR_MODE_NORMAL  0x00
#define PWR_MODE_LOW	 0x01
#define PWR_MODE_SUSPEND 0x02

#define OPR_MODE_SEL_BIT 0
#define OPR_MODE_LENGTH  4

#define OPR_MODE_CONFIG_MODE  0x00
#define OPR_MODE_ACCONLY 	  0x01
#define OPR_MODE_MAGONLY 	  0x02
#define OPR_MODE_GYROONLY 	  0x03
#define OPR_MODE_ACCMAG 	  0x04
#define OPR_MODE_ACCGYRO 	  0x05
#define OPR_MODE_MAGGYRO 	  0x06
#define OPR_MODE_AMG	 	  0x07
#define OPR_MODE_IMU	 	  0x08
#define OPR_MODE_COMPASS 	  0x09
#define OPR_MODE_M4G	 	  0x0A
#define OPR_MODE_NDOF_FMC_OFF 0x0B
#define OPR_MODE_NDOF 		  0x0C

#define DATA_RATE_SEL_BIT    4
#define DATA_RATE_SEL_LENGTH 3

#define FASTEST_MODE     0x10
#define GAME_MODE	 0x20
#define UI_MODE		 0x30
#define NORMAL_MODE      0x40

#define X_AXIS_REMAP_BIT  0
#define Y_AXIS_REMAP_BIT  2
#define Z_AXIS_REMAP_BIT  4
#define AXIS_REMAP_LENGTH 2

#define AXIS_REP_X 0x00
#define AXIS_REP_Y 0x01
#define AXIS_REP_Z 0x02

#define AXIS_SIGN_REMAP_BIT	   0
#define AXIS_SIGN_REMAP_LENGTH 3

#define ACC_RANGE_SEL_BIT	  0
#define ACC_RANGE_SEL_LENGTH  2
#define ACC_BW_SEL_BIT        2 //Auto controlled in fusion mode
#define ACC_BW_SEL_LENGTH     3 //Auto controlled in fusion mode
#define ACC_OPMODE_SEL_BIT    5 //Auto controlled in fusion mode
#define ACC_OPMODE_SEL_LENGTH 3 //Auto controlled in fusion mode

#define ACC_RANGE_2G  0x00
#define ACC_RANGE_4G  0x01 // Default
#define ACC_RANGE_8G  0x02
#define ACC_RANGE_16G 0x03

#define GYR_RANGE_SEL_BIT	  0 //Auto controlled in fusion mode
#define GYR_RANGE_SEL_LENGTH  3 //Auto controlled in fusion mode
#define GYR_BW_SEL_BIT        3 //Auto controlled in fusion mode
#define GYR_BW_SEL_LENGTH     3 //Auto controlled in fusion mode
#define GYR_OPMODE_SEL_BIT    6 //Auto controlled in fusion mode
#define GYR_OPMODE_SEL_LENGTH 2 //Auto controlled in fusion mode

#define MAG_RANGE_SEL_BIT	  0 //Auto controlled in fusion mode
#define MAG_RANGE_SEL_LENGTH  3 //Auto controlled in fusion mode
#define MAG_BW_SEL_BIT        3 //Auto controlled in fusion mode
#define MAG_BW_SEL_LENGTH     2 //Auto controlled in fusion mode
#define MAG_OPMODE_SEL_BIT    5 //Auto controlled in fusion mode
#define MAG_OPMODE_SEL_LENGTH 2 //Auto controlled in fusion mode

#define ACC_UNIT_SEL_BIT          0
#define ANGULAR_RATE_UNIT_SEL_BIT 1
#define EULER_ANGLE_UNIT_SEL_BIT  2
#define TEMP_UNIT_SEL_BIT         4
#define UNIT_SEL_LENGTH           1

#define ACC_UNIT_M_SSQ        0
#define ACC_UNIT_MG           1
#define ANGULAR_RATE_UNIT_DPS 0
#define ANGULAR_RATE_UNIT_RPS 1
#define EULER_ANGLE_UNIT_DEG  0
#define EULER_ANGLE_UNIT_RAD  1
#define TEMP_UNIT_C           0
#define TEMP_UNIT_F           1

Adafruit_SI1145 uv = Adafruit_SI1145();

void setup()
{
	ZigduinoRadio.begin(11);
	Serial.begin(115200);

	ZigduinoRadio.attachError(errHandle);
	ZigduinoRadio.attachTxDone(onXmitDone);

	inputString.reserve(20);

	// turn off LEDs
	pinMode(28, INPUT);
	pinMode(13, INPUT);
	pinMode(23, INPUT);
	pinMode(24, INPUT);

	stringComplete = true;
	closing = false;
	counter = 0;
	inputString = "";

	Wire.begin();
	BNO055_write(OPR_MODE, FASTEST_MODE | OPR_MODE_NDOF);

	humiditySensor.init();
	humiditySensor.Reset();

	for (int thisReading = 0; thisReading < numReadings; thisReading++) 
	{
		readings[thisReading] = 0;
	}

}

int seqNumTest = 0;
void loop()
{
	total = total - readings[readIndex];
	// read from the sensor:
	readings[readIndex] = vibration();
	// add the reading to the total:
	total = total + readings[readIndex];
	// advance to the next position in the array:
	readIndex = readIndex + 1;

	// if we're at the end of the array...
	if (readIndex >= numReadings) {
		// ...wrap around to the beginning:
		readIndex = 0;
	}

	// calculate the average:
	average = total / numReadings;
	// send it to the computer as ASCII digits


	//// handle incoming data
	while (ZigduinoRadio.available()) {
		// get the new byte:
		char inChar = (char)ZigduinoRadio.read();
		
		// add it to the inputString:
		inputString += inChar;
		// if the incoming character is a newline, set a flag
		// so the main loop can do something about it:

		if ((inChar == 123)){
			counter = 0;
			inputString = "";
		}


		if ((inChar == 125) || (inChar == ',')){
			tmpvalues[counter] = inputString.toInt();
			counter++;
			inputString = "";
		}

		if (inChar == 125)
			closing = true;

		if ((inChar == '\n') || (inChar == '\r'))
			if (closing && (counter == 4) && (!stringComplete)){
				//   if(closing&&(!stringComplete)){
				closing = false;
				counter = 0;
				values[0] = tmpvalues[0];
				values[1] = tmpvalues[1];
				values[2] = tmpvalues[2];
				values[3] = tmpvalues[3];
				stringComplete = true;
			}
			else{
				closing = false;
				counter = 0;
				inputString = "";
			}

			//      inputString = trim(inputString);
			//   int[] values = int(split(inputString, ","));
			//    if (values.length ==4) 
			//      stringComplete = true;
	}

	if (stringComplete) {

		Serial.print("received: ");
		Serial.print(values[0]);
		Serial.print(",");
		Serial.print(values[1]);
		Serial.print(",");
		Serial.print(values[2]);
		Serial.print(",");
		Serial.print(values[3]);
		Serial.println("");

		if ((values[0] == serverid) && ((values[1] == nodeid) || (values[1] == 8))){
			switch (values[2]){
			case 1:
				switch (values[3]){
				case 0:
					pinMode(11, INPUT);
					pinMode(27, INPUT);
					pinMode(8, INPUT);
					break;
				case 1:
					analogWrite(11, 0);
					analogWrite(27, 255);
					analogWrite(8, 255);
					pinMode(11, OUTPUT);
					pinMode(27, OUTPUT);
					pinMode(8, OUTPUT);
					break;
				case 2:
					analogWrite(11, 255);
					analogWrite(27, 0);
					analogWrite(8, 255);
					pinMode(11, OUTPUT);
					pinMode(27, OUTPUT);
					pinMode(8, OUTPUT);
					break;
				case 3:
					analogWrite(11, 0);
					analogWrite(27, 0);
					analogWrite(8, 255);
					pinMode(11, OUTPUT);
					pinMode(27, OUTPUT);
					pinMode(8, OUTPUT);
					break;
				case 4:
					analogWrite(11, 255);
					analogWrite(27, 255);
					analogWrite(8, 0);
					pinMode(11, OUTPUT);
					pinMode(27, OUTPUT);
					pinMode(8, OUTPUT);
					break;
				case 5:
					analogWrite(11, 0);
					analogWrite(27, 255);
					analogWrite(8, 0);
					pinMode(11, OUTPUT);
					pinMode(27, OUTPUT);
					pinMode(8, OUTPUT);
					break;
				case 6:
					analogWrite(11, 255);
					analogWrite(27, 0);
					analogWrite(8, 0);
					pinMode(11, OUTPUT);
					pinMode(27, OUTPUT);
					pinMode(8, OUTPUT);
					break;
				case 7:
					analogWrite(11, 0);
					analogWrite(27, 0);
					analogWrite(8, 0);
					pinMode(11, OUTPUT);
					pinMode(27, OUTPUT);
					pinMode(8, OUTPUT);
					break;
				}
				break;
			case 2:
				sprintf(bytes, "{%d,%d,%d,%d}\n\r", nodeid, serverid, 2, average);
				ZigduinoRadio.beginTransmission();
				ZigduinoRadio.write(bytes);
				ZigduinoRadio.endTransmission();
				break;
			case 3:
				sprintf(bytes, "{%d,%d,%d,%d}\n\r", nodeid, serverid, 3, analogRead(A3));
				ZigduinoRadio.beginTransmission();
				ZigduinoRadio.write(bytes);
				ZigduinoRadio.endTransmission();
				break;
			case 4:
				sprintf(bytes, "{%d,%d,%d,%d}\n\r", nodeid, serverid, 4, average);
				ZigduinoRadio.beginTransmission();
				ZigduinoRadio.write(bytes);
				ZigduinoRadio.endTransmission();
				break;
			case 5:
				sprintf(bytes, "{%d,%d,%d,%d}\n\r", nodeid, serverid, 5, (int)humiditySensor.MeasureHumidity());
				ZigduinoRadio.beginTransmission();
				ZigduinoRadio.write(bytes);
				ZigduinoRadio.endTransmission();
				break;
			case 6:
				sprintf(bytes, "{%d,%d,%d,%d}\n\r", nodeid, serverid, 6, seqNumTest);
				seqNumTest++;
				ZigduinoRadio.beginTransmission();
				ZigduinoRadio.write(bytes);
				ZigduinoRadio.endTransmission();
				break;
			case 7:
				int ir_level = uv.readIR();
				sprintf(bytes, "{%d,%d,%d,%d}\n\r", nodeid, serverid, 7, ir_level);
				ZigduinoRadio.beginTransmission();
				ZigduinoRadio.write(bytes);
				ZigduinoRadio.endTransmission();
				break;
			}
		}

		// clear the string:

		stringComplete = false;
	}

	//Serial.println(analogRead(A3));
	 //Serial.println(average);
	// Serial.println((int)humiditySensor.MeasureHumidity());
	//char scope_buf[50];
	//sprintf(scope_buf, "%d,%d,%d", vibration(), 0, 0);
	//Serial.println(scope_buf);
}

void errHandle(radio_error_t err)
{
	Serial.println();
	Serial.print("Error: ");
	Serial.print((uint8_t)err, 10);
	Serial.println();
}

void onXmitDone(radio_tx_done_t x)
{
	/*
	Serial.println();
	Serial.print("TxDone: ");
	Serial.print((uint8_t)x, 10);
	Serial.println();
	*/
}


int vibration(){
	float temp[3];
	BNO055_3_vec(LIA_DATA_X_LSB, &temp[0]);
	float vib = temp[0] * temp[0];
	vib += temp[1] * temp[1];
	vib += temp[2] * temp[2];
	return (int)log(vib);
	//Serial.print(vib);
	//Serial.println();
}

void BNO055_3_vec(int8_t addr, float *vec){
	Wire.beginTransmission(BNO055_I2C_ADDR);
	Wire.write(addr);
	Wire.endTransmission(false);

	Wire.requestFrom(BNO055_I2C_ADDR, 6, false);
	int16_t b_data[6];
	for (int i = 0; i < 6; i++)
		b_data[i] = Wire.read();

	vec[0] = b_data[1] << 8 | b_data[0];
	vec[1] = b_data[3] << 8 | b_data[2];
	vec[2] = b_data[5] << 8 | b_data[4];
}

int BNO055_write(int addr, int data){
	Wire.beginTransmission(BNO055_I2C_ADDR);
	Wire.write(addr);
	Wire.write(data);
	Wire.endTransmission(true);

	return 1;
}