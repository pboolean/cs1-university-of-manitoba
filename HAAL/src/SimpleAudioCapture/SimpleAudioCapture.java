/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SimpleAudioCapture;

/**
 *
 * @author cnaldaba
 */

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

import sun.audio.AudioPlayer;
import sun.audio.AudioStream;

public class SimpleAudioCapture {



    public SimpleAudioCapture() {

    }

    public void playAudio(String fileName) {
        // open the sound file as a Java input stream
        String output = System.getProperty("user.dir") + "\\AudioFiles\\" + fileName;
        //String gongFile = "/Users/al/DevDaily/Projects/MeditationApp/resources/gong.au";
        InputStream in;
        try {
            in = new FileInputStream(output);
            // create an audiostream from the inputstream
            AudioStream audioStream;
            try {
                audioStream = new AudioStream(in);
                // play the audio clip with the audioplayer class
                AudioPlayer.player.start(audioStream);
            } catch (IOException ex) {
                Logger.getLogger(SimpleAudioCapture.class.getName()).log(Level.SEVERE, null, ex);
            }

        } catch (FileNotFoundException ex) {
            Logger.getLogger(SimpleAudioCapture.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    
    
    
    

 
}
