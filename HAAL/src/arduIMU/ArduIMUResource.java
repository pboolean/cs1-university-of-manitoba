
import java.util.logging.Level;
import java.util.logging.Logger;
import jssc.SerialPort;
import jssc.SerialPortEvent;
import jssc.SerialPortEventListener;
import jssc.SerialPortException;
import jssc.SerialPortTimeoutException;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Paul
 */
public class ArduIMUResource
{

    private static final String SERIAL_PORT_ID = "COM9";
    private static final int BIT_RATE = 115200;
    private static final int TIMEOUT = 2000;
    private static SerialPort serialPort;

    private static void garbageReads(SerialPort serialPort) throws SerialPortException
    {
        try
            {
                System.out.println("Gabage 1");
                String garbage1 = readSerialLine(serialPort, TIMEOUT);
            } catch (SerialPortTimeoutException ex)
            {
                // do nothing
            }

            try
            {
                System.out.println("Gabage 2");
                String garbage2 = readSerialLine(serialPort, TIMEOUT);
            } catch (SerialPortTimeoutException ex)
            {
                // do nothing
            }
             try
            {
                System.out.println("Gabage 3");
                String garbage3 = readSerialLine(serialPort, TIMEOUT);
            } catch (SerialPortTimeoutException ex)
            {
                // do nothing
            }
    }
    
    private static void initSerialPort()
    {
        serialPort = new SerialPort(SERIAL_PORT_ID);

        try
        {
            serialPort.openPort();//Open port

            garbageReads(serialPort);            
            
            if (serialPort.setParams(BIT_RATE, 8, 1, 0))//Set params
            {
                int mask = SerialPort.MASK_RXCHAR + SerialPort.MASK_CTS + SerialPort.MASK_DSR;//Prepare mask
//                serialPort.setEventsMask(mask);//Set mask
//                serialPort.addEventListener(new SerialListener(serialPort));//Add SerialPortEventListener
            } else
            {
                System.out.println("Invalid Serial Port parameters");
            }
        } catch (SerialPortException ex)
        {
            System.err.println("Error opening serial port.");
            ex.printStackTrace();
            silentClose();
            System.exit(1);
        }

        Runtime.getRuntime()
                .addShutdownHook(new Thread()
                {

                    public void run()
                    {
                        silentClose();
                    }
                }
                );
    }

    private static void silentClose()
    {
        try
        {
            serialPort.closePort();
        } catch (SerialPortException ex)
        {
            ex.printStackTrace();

        }

    }

    private static class SerialListener implements SerialPortEventListener
    {

        SerialPort mPort;
        int dataSize;
        public static final int PREAMBLE_SIZE = 3;

        public SerialListener(SerialPort port)
        {
            mPort = port;
        }

        @Override
        public void serialEvent(SerialPortEvent serialPortEvent)
        {
            int val = serialPortEvent.getEventValue();
            if (serialPortEvent.isRXCHAR() && val > 0)
            {
                System.out.println("Received " + val + " bytes");

                try
                {

                    String dataString = readSerialLine(mPort, TIMEOUT);
                    System.out.println("Received Event: " + dataString);

                } catch (SerialPortException e)
                {
                    e.printStackTrace();

                } catch (SerialPortTimeoutException e)
                {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        }
    }

    public enum ArduImuId
    {

        SINK(0),
        TOOTHPASTE(1),
        TOOTHBRUSH(2),
        CUP(3);

        private int address;

        private ArduImuId(int address)
        {
            this.address = address;
        }

        public int getAddress()
        {
            return address;
        }
    }

    public enum Query
    {

        IN_CUP(0), // toothbrush
        IN_HAND(1), // toothpaste, toothbrush
        IS_SQUEEZED(2), // toothpaste
        BRUSHING_TEETH(3), // toothbrush
        WATER_FLOWING(4); // toothbrush, sink// toothbrush, sink

        private int queryId;

        private Query(int queryId)
        {
            this.queryId = queryId;
        }

        public int getAddress()
        {
            return queryId;
        }
    }

    public enum Action
    {

        LED_ON(0),
        LED_OFF(1);

        private int actionId;

        private Action(int actionId)
        {
            this.actionId = actionId;
        }

        public int getAddress()
        {
            return actionId;
        }
    }

    public static void doAction(ArduImuId arduImuId, Action action)
    {
        // write an action to a particular ArduIMU
        if (null == serialPort)
        {
            initSerialPort();
        }
        String[] actionStrings = new String[]
        {
            "0",
            "" + arduImuId.address,
            "" + action.actionId,
            ""
        };

        String actionPacket = "{" + String.join(",", actionStrings) + "}\n";

        try
        {
            serialPort.writeString(actionPacket);
            System.out.println("Sending: " + actionPacket);
        } catch (SerialPortException ex)
        {
            ex.printStackTrace();
        }
    }

    public static boolean querySensor(ArduImuId arduImuId, Query query)
    {
        if (null == serialPort)
        {
            initSerialPort();
        }

        String[] queryStrings = new String[]
        {
            "0",
            "" + arduImuId.address,
            "" + query.queryId,
            ""
        };

        String queryPacket = "{" + String.join(",", queryStrings) + "}\n";

        try
        {
            serialPort.writeString(queryPacket);
            System.out.println("Sending: " + queryPacket);

//            String lol = readSerialLine(serialPort, TIMEOUT);
            String response = readSerialLine(serialPort, TIMEOUT);
            System.out.println("Received: " + response);
        } catch (SerialPortException ex)
        {
            ex.printStackTrace();
        } catch (SerialPortTimeoutException ex)
        {
            ex.printStackTrace();
        }

        return false;
    }

    private static String readSerialLine(SerialPort mSerialPort, int timeout) throws SerialPortException, SerialPortTimeoutException
    {
        String line = "";

        char readChar = mSerialPort.readString(1, timeout).charAt(0);
        while (readChar != '\n')
        {
            line += readChar;
            readChar = mSerialPort.readString(1, timeout).charAt(0);
        }
        return line;
    }
}
