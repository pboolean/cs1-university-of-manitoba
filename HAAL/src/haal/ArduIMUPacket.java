/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package haal;
/**
 *
 * @author Paul
 */
public class ArduIMUPacket
{

    public enum PacketType
    {

        COMMAND,
        QUERY
    }

    public final ArduIMUResource.ArduImuId sender;
    public final ArduIMUResource.ArduImuId receiver;
    public final PacketType packetType;
    public final ArduIMUResource.Action command;
    public final ArduIMUResource.Query query;
    public final int argument;

    /**
     * Construct a packet from incoming data
     *
     * @param data Should be comma-separated String
     */
    public ArduIMUPacket(String data) throws NumberFormatException
    {
        String[] fields = data.substring(1, data.lastIndexOf("}")).split(",");
        int senderId = Integer.parseInt(fields[0]);
        sender = ArduIMUResource.ArduImuId.getValue(senderId);

        int receiverId = Integer.parseInt(fields[1]);
        receiver = ArduIMUResource.ArduImuId.getValue(receiverId);

        
        ArduIMUResource.Query queryTemp;
        ArduIMUResource.Action commandTemp;
        int actionId = Integer.parseInt(fields[2]);
        try
        {
            commandTemp = ArduIMUResource.Action.getValue(actionId);
            queryTemp = ArduIMUResource.Query.NONE;
        } catch (IllegalArgumentException ex)
        {
            queryTemp = ArduIMUResource.Query.getValue(actionId);
            commandTemp = ArduIMUResource.Action.NONE;
        }
        query = queryTemp;
        command = commandTemp;
        
        if (ArduIMUResource.Action.NONE == command)
        {
            packetType = PacketType.QUERY;
        } else
        {
            packetType = PacketType.COMMAND;
        }

        argument = Integer.parseInt(fields[3]);
    }
}
