/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package haal;
/**
 *
 * @author Paul
 */
public class ArduIMUPacketResult
{

    private static final int TOOTHBRUSH_WATER_THRESH = 30;
    private static final int SINK_WATER_THRESH = 55;
    private static final int SQUEEZE_THRES = 900;
    
    private static final int INHAND_THRES = 5;
    
    private static final int BRUSH_THRES = 8;
    private static final int CUP_THRES = 300; //300 for main area

    public static boolean evaluateWater(int waterVal, ArduIMUResource.ArduImuId arduImuId)
    {
        System.out.println("Water flow: " + waterVal);
        switch (arduImuId)
        {
            case TOOTHBRUSH:
               return waterVal > TOOTHBRUSH_WATER_THRESH;

            case SINK:
                return waterVal > SINK_WATER_THRESH;
            default:
                return false;
        }
    }
    
    public static boolean evaluateInCup(int cupVal, ArduIMUResource.ArduImuId arduImuId)
    {
        if(cupVal < CUP_THRES){
            return true;
        } else{
            return false;
        }
    }
    
    public static boolean evaluateInHand(int handVal, ArduIMUResource.ArduImuId arduImuId)
    {
        if (handVal > INHAND_THRES){
            return true;
        } else{
            return false;
        }
        
     
    }
    
    public static boolean evaluateIsSqueezed(int squeezedVal, ArduIMUResource.ArduImuId arduImuId)
    {
        if (squeezedVal < SQUEEZE_THRES)
            return true;
        else 
            return false;
    }
    
    public static boolean evaluateBrushingTeeth(int brushingVal, ArduIMUResource.ArduImuId arduImuId)
    {
        if (brushingVal > BRUSH_THRES){
            return true;
        } else{
            return false;
        }
        
    }
}
