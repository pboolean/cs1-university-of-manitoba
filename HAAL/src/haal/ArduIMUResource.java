package haal;
import java.util.logging.Level;
import java.util.logging.Logger;
import jssc.SerialPort;
import jssc.SerialPortEvent;
import jssc.SerialPortEventListener;
import jssc.SerialPortException;
import jssc.SerialPortTimeoutException;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Paul
 */
public class ArduIMUResource
{

    private static final String SERIAL_PORT_ID = "COM10";
    private static final int BIT_RATE = 115200;
    private static final int TIMEOUT = 200;
    private static final int RETRIES = 20; // max times to retry before giving up
    private static SerialPort serialPort;

    private static void garbageReads(SerialPort serialPort) throws SerialPortException
    {
        try
        {
            
            String garbage1 = readSerialLine(serialPort, TIMEOUT);
            System.out.println("Garbage 1: " + garbage1);
        } catch (SerialPortTimeoutException ex)
        {
            // do nothing
        }

        try
        {
            String garbage2 = readSerialLine(serialPort, TIMEOUT);
            System.out.println("Garbage 2: " + garbage2);
        } catch (SerialPortTimeoutException ex)
        {
            // do nothing
        }
        try
        {
            String garbage3 = readSerialLine(serialPort, TIMEOUT);
            System.out.println("Garbage 3: " + garbage3);
        } catch (SerialPortTimeoutException ex)
        {
            // do nothing
        }
    }

    private static void initSerialPort()
    {
        serialPort = new SerialPort(SERIAL_PORT_ID);

        try
        {
            serialPort.openPort();//Open port

          garbageReads(serialPort);

            if (serialPort.setParams(BIT_RATE, 8, 1, 0))//Set params
            {
                int mask = SerialPort.MASK_RXCHAR + SerialPort.MASK_CTS + SerialPort.MASK_DSR;//Prepare mask
//                serialPort.setEventsMask(mask);//Set mask
//                serialPort.addEventListener(new SerialListener(serialPort));//Add SerialPortEventListener
            } else
            {
                System.out.println("Invalid Serial Port parameters");
            }
        } catch (SerialPortException ex)
        {
            System.err.println("Error opening serial port.");
            ex.printStackTrace();
            silentClose();
            System.exit(1);
        }

        Runtime.getRuntime()
                .addShutdownHook(new Thread()
                {

                    public void run()
                    {
                        silentClose();
                    }
                }
                );
    }

    private static void silentClose()
    {
        try
        {
            serialPort.closePort();
        } catch (SerialPortException ex)
        {
            ex.printStackTrace();

        }

    }

//    private static class SerialListener implements SerialPortEventListener
//    {
//
//        SerialPort mPort;
//        int dataSize;
//        public static final int PREAMBLE_SIZE = 3;
//
//        public SerialListener(SerialPort port)
//        {
//            mPort = port;
//        }
//
//        @Override
//        public void serialEvent(SerialPortEvent serialPortEvent)
//        {
//            int val = serialPortEvent.getEventValue();
//            if (serialPortEvent.isRXCHAR() && val > 0)
//            {
//                System.out.println("Received " + val + " bytes");
//
//                try
//                {
//
//                    String dataString = readSerialLine(mPort, TIMEOUT);
//                    System.out.println("Received Event: " + dataString);
//
//                } catch (SerialPortException e)
//                {
//                    e.printStackTrace();
//
//                } catch (SerialPortTimeoutException e)
//                {
//                    // TODO Auto-generated catch block
//                    e.printStackTrace();
//                }
//            }
//        }
//    }
    public enum ArduImuId
    {

        NONE(99),
        CORE(19),
        SINK(1),
        TOOTHPASTE(2),
        TOOTHBRUSH(3),
        CUP(4),
        GLOBAL(8);

        private int address;

        private static ArduImuId[] vals = null;

        private ArduImuId(int address)
        {
            this.address = address;
        }

        public int getAddress()
        {
            return address;
        }

        public static ArduImuId getValue(int address) throws IllegalArgumentException
        {
            if (null == vals)
            {
                vals = values();
            }

            for (int i = 0; i < vals.length; i++)
            {
                if (vals[i].address == address)
                {
                    return vals[i];
                }
            }
            
            throw new IllegalArgumentException(address + " is not a valid ArduImuId");
        }

    }

    public enum Query
    {

        NONE(99),
        IN_CUP(7), // toothbrush
        IN_HAND(2), // toothpaste, toothbrush
        IS_SQUEEZED(3), // toothpaste
        BRUSHING_TEETH(4), // toothbrush
        WATER_FLOWING(5), // toothbrush, sink
        TEST(6);

        private int queryId;
        private static Query[] vals = null;

        private Query(int queryId)
        {
            this.queryId = queryId;
        }

        public int getAddress()
        {
            return queryId;
        }

        public static Query getValue(int queryId) throws IllegalArgumentException
        {
            if (null == vals)
            {
                vals = values();
            }

            for (int i = 0; i < vals.length; i++)
            {
                if (vals[i].queryId == queryId)
                {
                    return vals[i];
                }
            }
            throw new IllegalArgumentException(queryId + " is not recognized as a valid query.");
        }
    }

    public enum Action
    {

        NONE(99),
        LED_ON(6),
        LED_OFF(0);

        private int actionId;

        private static Action[] vals = null;

        private Action(int actionId)
        {
            this.actionId = actionId;
        }

        public int getActionId()
        {
            return actionId;
        }

        public static Action getValue(int actionId) throws IllegalArgumentException
        {
            if (null == vals)
            {
                vals = values();
            }

            for (int i = 0; i < vals.length; i++)
            {
                if (vals[i].actionId == actionId)
                {
                    return vals[i];
                }
            }
            throw new IllegalArgumentException(actionId + " is not recognized as a valid action.");
        }

    }

    public static void doAction(ArduImuId arduImuId, Action action)
    {
        // write an action to a particular ArduIMU
        if (null == serialPort)
        {
            initSerialPort();
        }
        String[] actionStrings = new String[]
        {
            "19",
            "" + arduImuId.address,
            "1" , // command 1 = LED state
            ""   + action.actionId //actionId determines on or off
        };
        


        String actionPacket = "{" + String.join(",", actionStrings) + "}\r\n" ;

        try
        {
            //serialPort.writeString(actionPacket );
            serialPort.writeBytes(actionPacket.getBytes());
            System.out.println("Sending: " + actionPacket);
        } catch (SerialPortException ex)
        {
            ex.printStackTrace();
        }
    }
    
    private static boolean querySensorRetry(ArduImuId arduImuId, Query query, int retries)
    {
        if (null == serialPort)
        {
            initSerialPort();
        }

        String[] queryStrings = new String[]
        {
            "19",
            "" + arduImuId.address,
            "" + query.queryId,
            "0" 
        };

        String queryPacket = "{" + String.join(",", queryStrings) + "}\r\n";

        boolean queryResult = false;
        try
        {
            serialPort.writeString(queryPacket);
//            System.out.println("Sending: " + queryPacket);

//            String lol = readSerialLine(serialPort, TIMEOUT);
            String response = readSerialLine(serialPort, TIMEOUT);
            ArduIMUPacket receivedPacket = new ArduIMUPacket(response);
            
            System.out.println("received: " + response);

            if (receivedPacket.sender == arduImuId && receivedPacket.packetType == ArduIMUPacket.PacketType.QUERY)
            {

                // decide whether to return true or false
                switch (receivedPacket.query)
                {
                    case WATER_FLOWING:
                        queryResult = ArduIMUPacketResult.evaluateWater(receivedPacket.argument, receivedPacket.sender);
                        break;

                    case IN_CUP:
                        queryResult = ArduIMUPacketResult.evaluateInCup(receivedPacket.argument, receivedPacket.sender);
                        break;

                    case BRUSHING_TEETH:
                        queryResult = ArduIMUPacketResult.evaluateBrushingTeeth(receivedPacket.argument, receivedPacket.sender);
                        break;

                    case IN_HAND:
                        queryResult = ArduIMUPacketResult.evaluateInHand(receivedPacket.argument, receivedPacket.sender);
                        break;

                    case IS_SQUEEZED:
                        queryResult = ArduIMUPacketResult.evaluateIsSqueezed(receivedPacket.argument, receivedPacket.sender);
                        break;

                    case NONE:
                    default:
                        queryResult = false;
                }

            }

        } catch (Exception ex)
        {
//            ex.printStackTrace();
            int queryCount = retries + 1;
            
            if (queryCount > RETRIES)
            {
                System.err.println("TOO MANY FAILURES(" + queryCount + ").");
//                ex.printStackTrace();
                return false;
            }
            
//            System.err.println("Re-transmitting: " + queryCount + "/" + RETRIES);
            
            return querySensorRetry(arduImuId, query, queryCount);
        }

        return queryResult;
    }

    public static boolean querySensor(ArduImuId arduImuId, Query query)
    {
        return querySensorRetry(arduImuId, query, 0);
    }

    private static String readSerialLine(SerialPort mSerialPort, int timeout) throws SerialPortException, SerialPortTimeoutException
    {
        String line = "";

        char readChar = mSerialPort.readString(1, timeout).charAt(0);
        while (readChar != '\n')
        {
            line += readChar;
            readChar = mSerialPort.readString(1, timeout).charAt(0);
        }
        return line;
    }
}
