/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package haal;

import SimpleAudioCapture.SimpleAudioCapture;
import haal.ArduIMUResource.Action;
import haal.ArduIMUResource.ArduImuId;
import haal.ArduIMUResource.Query;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.media.Media;
import javax.swing.Timer;
import javafx.scene.media.MediaPlayer;
import java.net.MalformedURLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 *
 * @author cnaldaba
 */
public class FXMLDocumentController implements Initializable {

    private MediaPlayer audio;

    private final int BRUSH_TIME = 20000; //One minute teeth brushing = 60000
    private final int REPEATED_AUDIO_DELAY = 10000; // Delay time to play repeated audio
    private final int SPAT_DELAY = 3000; // Delay time to play repeated audio
    private final int IMU_DELAY = 20000;

    private SimpleAudioCapture player = new SimpleAudioCapture();
    private ArduIMUResource a = new ArduIMUResource();

    private Boolean isTaskComplete, repeatAudio, startOfStep, timeFlag, isAudioPlaying;
    private int stepNumber;

    private Timer time, audioTimer;

    @FXML
    private Label label;
    
    private static final int INIT_STEP = 1;

    @FXML
    private void OpenCreateWindow(ActionEvent e){
         FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("HAAL_CreateTask.fxml"));
        Parent root1;
        try {
            root1 = (Parent) fxmlLoader.load();
            Stage stage = new Stage();
            stage.initModality(Modality.APPLICATION_MODAL);

            stage.setScene(new Scene(root1));
            stage.show();
        } catch (IOException ex) {

        }
    }
    @FXML
    private void handleButtonAction(ActionEvent event) {
        System.out.println("Start teethbrushing");
        
     
        try {
            StartTask();
        } catch (InterruptedException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        stepNumber = INIT_STEP;
        startOfStep = true;

        isTaskComplete = false;
        repeatAudio = false;
        timeFlag = false;
        isAudioPlaying = false;

        /*time = new Timer(delay,taskPerformer);
         time.start(); */
        audioTimer = new Timer(REPEATED_AUDIO_DELAY, RepeatedAudioTask);
        audioTimer.start();

    }
    
    private void ledSent(ArduImuId id, Action a){
         int i = 0;
          while(i < 10){
         ArduIMUResource.doAction(id, a );
             try {
                 //System.out.println("Turned on toothbrush LED");
                 Thread.sleep(500);
             } catch (InterruptedException ex) {
                 Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
             }
         i++;
        }
    }

    private void StartTask() throws InterruptedException {

       
        //Initiate first step:
       ledSent(ArduImuId.TOOTHBRUSH, Action.LED_ON);
        while (!isTaskComplete) {
            Thread.sleep(500);
//delay(IMU_DELAY); // To ensure we recieve everything ok
            
            if (startOfStep) { //State instruction at start of step
                startOfStep = false;
                player.playAudio(getAudio(stepNumber));

                if (stepNumber == 5) { //Brushing teeth step, start one minute timer
                    time = new Timer(BRUSH_TIME, taskPerformer);
                    time.start();

                } else if (stepNumber == 6) { // delay to wait for person to spit
                    timeFlag = false;
                    time = new Timer(SPAT_DELAY, taskPerformer);
                    time.start();
                }
            }

            switch (stepNumber) {
                case 1: //Pick up toothbrush
                    if (a.querySensor(ArduImuId.TOOTHBRUSH, Query.IN_HAND)) {
                        taskCompleted();
                        System.out.println("Picked up toothbrush");
                        ledSent(ArduImuId.TOOTHPASTE,Action.LED_ON);
                        
                        System.out.println("Turned on toothpaste LED");
                    } else {
                        repeatAudio = true; //Depends on cycling time --> counter?
                    }
                    break;
                case 2://Pick up toothpaste

                    if (a.querySensor(ArduImuId.TOOTHPASTE, Query.IN_HAND)) {
                        taskCompleted();
                        System.out.println("Picked up toothpaste");
                     
                    } else {
                        repeatAudio = true;
                    }

                    break;
                case 3: //Put toothpaste onto toothbrush

                    if (a.querySensor(ArduImuId.TOOTHPASTE, Query.IS_SQUEEZED)) {
                        taskCompleted();
                        System.out.println("Squeezed toothpaste onto toothbrush");
                    } else {
                        repeatAudio = true;
                    }
                    break;
                case 4: //Put toothpaste down

                    if ((a.querySensor(ArduImuId.TOOTHPASTE, Query.IN_HAND))) { //NOT
                        taskCompleted();
                        System.out.println("Placed toothpaste down");
                       
                        ledSent(ArduImuId.TOOTHPASTE,Action.LED_OFF);
                        ledSent(ArduImuId.TOOTHBRUSH,Action.LED_ON);
                        System.out.println("Turned off toothpaste LED, but toothbrush LED is on");
                       
                    } else {
                        repeatAudio = true;
                    }
                    break;
                case 5: //Brush your teeth

                    if ((a.querySensor(ArduImuId.TOOTHBRUSH, Query.BRUSHING_TEETH) && timeFlag)) {
                        taskCompleted();
                        timeFlag = false;
                        System.out.println("Finished brushing teeth");
                    } else {
                        repeatAudio = true;
                    }
                    break;
                case 6: //spit into the sink

                    if (timeFlag) {
                        System.out.println("Spat into sink");
           
                        ledSent(ArduImuId.SINK,Action.LED_ON);
                        taskCompleted();
                        System.out.println("Sink LED on");
                    }

                    break;
                case 7: // turn on the facet

                    if (a.querySensor(ArduImuId.SINK, Query.WATER_FLOWING)) {
                        taskCompleted();
                        System.out.println("Turn on facet");
                    } else {
                        repeatAudio = true;
                    }
                    break;
                case 8:// rinse your toothbrush
                    if (a.querySensor(ArduImuId.TOOTHBRUSH, Query.WATER_FLOWING)) {
                        taskCompleted();
                        System.out.println("Rinsed toothbrush");
                    } else {
                        repeatAudio = true;
                    }
                    break;
                case 9: // turn off the facet
                    if ((a.querySensor(ArduImuId.SINK, Query.WATER_FLOWING))) { //NOT
                        taskCompleted();
                        
                        ledSent(ArduImuId.SINK,Action.LED_OFF);
                        ledSent(ArduImuId.CUP, Action.LED_ON);
                        System.out.println("Turned off facet and sink LED is turned off");
                    } else {
                        repeatAudio = true;
                    }
                    break;
                case 10: // put away your toothbrush
                    if ((a.querySensor(ArduImuId.TOOTHBRUSH, Query.IN_HAND))) { //NOT
                        taskCompleted();
                      
                        ledSent(ArduImuId.TOOTHBRUSH,Action.LED_OFF);
                        ledSent(ArduImuId.CUP,Action.LED_OFF);
                        isTaskComplete = true;
                        System.out.println("Put away toothbrush and LEDs are turned off");
                    } else {
                        repeatAudio = true;
                    }
                    break;

                default:
                    break;

            }

        }
        System.out.println("Task  completed");
        // Play good job!
        playAudio("good_job_hq.wav");

        ledSent(ArduImuId.GLOBAL, Action.LED_OFF);
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("HAAL_TaskCompleted.fxml"));
        Parent root1;
        try {
            root1 = (Parent) fxmlLoader.load();
            Stage stage = new Stage();
            stage.initModality(Modality.APPLICATION_MODAL);

            stage.setScene(new Scene(root1));
            stage.show();
        } catch (IOException ex) {

        }

    }

    private void delay(int milliseconds) {
        try {
            Thread.sleep(milliseconds);                 //1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
    }

    public String getAudio(int stepNumber) {
        String audioFile = "Audio_" + stepNumber + ".wav";
        return audioFile;
    }

    // Reinitiate the step parameters after person completes task
    public void taskCompleted() {
        stepNumber++;
        repeatAudio = false;
        startOfStep = true;
    }

    /**
     * Called after defined BRUSH_TIME to complete toothbrushing step
     */
    public ActionListener taskPerformer = new ActionListener() {

        @Override
        public void actionPerformed(java.awt.event.ActionEvent e) {

            if ((stepNumber == 5) || (stepNumber == 6)) {
                timeFlag = true;
                System.out.println("Timer elapsed");
            }
            time.stop();
        }
    };

    /**
     * This is delayed by REPEATED_AUDIO_DELAY to play audio instructions
     */
    public ActionListener RepeatedAudioTask = new ActionListener() {

        @Override
        public void actionPerformed(java.awt.event.ActionEvent e) {

            if (repeatAudio) {
                System.out.println("Replayed: :" + getAudio(stepNumber));
                repeatAudio = false;
                startOfStep = true;
            }

        }
    };

    /**
     * Plays Audio instruction accordingly via filename
     *
     * @param fileName
     */
    private void playAudio(String fileName) {
        String output = System.getProperty("user.dir") + "\\AudioFiles\\" + fileName;

        // Media media = new Media(output);
        //audio = new MediaPlayer(media);
        MediaPlayer mediaPlayer;
        try {
            mediaPlayer = createMediaPlayer(output);
            mediaPlayer.play();
            mediaPlayer.setOnEndOfMedia(new Runnable() {
                @Override
                public void run() {
                    System.out.println("Audio is finished");
                    isAudioPlaying = false;
                }
            });
        } catch (MalformedURLException ex) {
            //Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * creates a media player using a file from the given filename path and
     * tracks the status of playing the file via the status label
     */
    private MediaPlayer createMediaPlayer(final String filename) throws MalformedURLException {
        File file = new File(filename);

        final String mediaLocation = file.toURI().toURL().toString();
        Media media = new Media(mediaLocation);
        MediaPlayer mediaPlayer = new MediaPlayer(media);

        return mediaPlayer;
    }

}
