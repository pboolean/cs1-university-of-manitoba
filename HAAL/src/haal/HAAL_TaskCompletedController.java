/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package haal;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author cnaldaba
 */
public class HAAL_TaskCompletedController implements Initializable {

    /**
     * Initializes the controller class.
     */
    @FXML
    Button closeButton;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    @FXML
    private void closeButtonAction() {
        // get a handle to the stage
       Stage stage = (Stage) closeButton.getScene().getWindow();
       stage.close();
        // do what you have to do
        
        //Platform.exit();
//stage.close();
    }
}
