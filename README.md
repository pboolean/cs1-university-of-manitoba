# README #

CS1- University of Manitoba

### What's Here? ###

* DementiaHack3 contains the code that runs on the sensor modules (each has a different ID, but are otherwise identical)
* HAAL contains the server code that executes the tooth-brushing process
* RFReceiverDementiaHack contains the code that runs on the RF module that allows the pc to interact with the sensor network.