#include <ZigduinoRadio.h>
int rfChannel = 11;


void setup()
{
  Serial.begin(115200);
  
 // Serial.println("ArduIMU V4 Receiver...");
  
  ZigduinoRadio.begin(rfChannel);
  ZigduinoRadio.setParam(RP_TXPWR(3));
  ZigduinoRadio.setParam(RP_DATARATE(MOD_OQPSK_1000));
  
//  Serial.println("ArduIMU V4 :: init done.");
}

void loop()
{
  if (ZigduinoRadio.available())
  {
      while(ZigduinoRadio.available())
      Serial.write(ZigduinoRadio.read());
  }
  if (Serial.available())
  {
      while(Serial.available())
      ZigduinoRadio.write(Serial.read());
  }
}

